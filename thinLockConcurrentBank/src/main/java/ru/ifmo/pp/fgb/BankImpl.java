package ru.ifmo.pp.fgb;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Bank implementation.
 * <p/>
 *
 * @author Shalamov
 */
public class BankImpl implements Bank {
    /**
     * An array of accounts by index.
     */
    private final Account[] accounts;

    /**
     * Creates new bank instance.
     *
     * @param n the number of accounts (numbered from 0 to n-1).
     */
    public BankImpl(int n) {
        accounts = new Account[n];
        for (int i = 0; i < n; i++) {
            accounts[i] = new Account();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNumberOfAccounts() {
        return accounts.length;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getAmount(int index) {
        accounts[index].lock.lock();
        long amount = accounts[index].amount;
        accounts[index].lock.unlock();
        return amount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTotalAmount() {
        long sum = 0;
        for (Account account : accounts) {
            account.lock.lock();
            sum += account.amount;
        }
        for (int i = accounts.length - 1; i >= 0; --i) {
            accounts[i].lock.unlock();
        }
        return sum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long deposit(int index, long amount) {
        if (amount <= 0)
            throw new IllegalArgumentException("Invalid amount: " + amount);
        if (index < 0 || index >= accounts.length)
            throw new IndexOutOfBoundsException("invalid index: " + index);
        //accounts[index].lock.lock();
        Account account = accounts[index];
        account.lock.lock();
        if (amount > MAX_AMOUNT || account.amount + amount > MAX_AMOUNT) {
            account.lock.unlock();
            throw new IllegalStateException("Overflow");
        }
        account.amount += amount;
        long newAmount = account.amount;
        account.lock.unlock();
        return newAmount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long withdraw(int index, long amount) {
        if (amount <= 0)
            throw new IllegalArgumentException("Invalid amount: " + amount);
        if (index < 0 || index >= accounts.length)
            throw new IndexOutOfBoundsException("invalid index: " + index);
        Account account = accounts[index];
        account.lock.lock();
        if (account.amount - amount < 0) {
            account.lock.unlock();
            throw new IllegalStateException("Underflow");
        }
        account.amount -= amount;
        long newAmount = account.amount;
        account.lock.unlock();
        return newAmount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transfer(int fromIndex, int toIndex, long amount) {
        if (amount <= 0)
            throw new IllegalArgumentException("Invalid amount: " + amount);
        if (fromIndex == toIndex)
            throw new IllegalArgumentException("fromIndex == toIndex");
        if (fromIndex < 0 || fromIndex >= accounts.length || toIndex < 0 || toIndex >= accounts.length)
            throw new IndexOutOfBoundsException("invalid indexes: " + fromIndex + " " + toIndex);

        int first = Math.min(fromIndex, toIndex);
        int second = Math.max(fromIndex, toIndex);

        accounts[first].lock.lock();
        accounts[second].lock.lock();

        Account from = accounts[fromIndex];
        Account to = accounts[toIndex];
        if (amount > from.amount) {
            accounts[second].lock.unlock();
            accounts[first].lock.unlock();
            throw new IllegalStateException("Underflow");
        } else if (amount > MAX_AMOUNT || to.amount + amount > MAX_AMOUNT) {
            accounts[second].lock.unlock();
            accounts[first].lock.unlock();
            throw new IllegalStateException("Overflow");
        }
        from.amount -= amount;
        to.amount += amount;

        accounts[second].lock.unlock();
        accounts[first].lock.unlock();
    }

    /**
     * Private account data structure.
     */
    private static class Account {
        /**
         * Amount of funds in this account.
         */
        long amount;

        /**
         * Lock for thread-safety
         */
        Lock lock;

        /**
         * Creates new Account instance.
         */
        public Account() {
            amount = 0;
            lock = new ReentrantLock();
        }
    }
}
